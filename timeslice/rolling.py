# vim: set tabstop=4
# rolling.py
#!/usr/bin/env python3

""" Functionality to slice data sets along the sampling direction """

# Copyright (C) 2019 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#' % Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#' % 06.07.2019
import numpy as np
import matplotlib.pyplot as plt

from collections.abc import Iterable

def rollingtail_iter(tail, *, stop, start=None, step=1, horizon=None):
    """ Generator of indexes of a tailing rolling window

        Arguments
        ---------
        tail : int
            Length of the tail.

        Keyword arguments
        -----------------
        stop : int
            Last index of the head, + 1. If you want an infinite generator
            use None, or :py:class:`numpy.inf`.
        start : int, optional (default None)
            Initial index of the head. If not given, the default head starts
            at index given in length of the tail, that is the the tail spans
            index 0 to lenght of tail - 1.
        step : int, optional (default 1)
            The increment to the index of the head between consecutive calls.
        horizon : int, optional (default None)
            An index that is horizon indexes in front of the head of the tail.

        Returns
        -------
        tail_idx: :py:class:`numpy.ndarray`
            Indeces of the tail, with shape (tail,).
        target_idx: int, optional
            If horizon is not None, it contains the index of the horizon
            observation.

        Examples
        --------
        >>> [idx for idx in rollingtail_iter(3, stop=10)]
        [array([0, 1, 2]), array([1, 2, 3]), array([2, 3, 4]), array([3, 4, 5]), array([4, 5, 6]), array([5, 6, 7]), array([6, 7, 8]), array([7, 8, 9])]

        >>> [idx for idx in rollingtail_iter(3, stop=13, horizon=2)]
        [(array([0, 1, 2]), 4), (array([1, 2, 3]), 5), (array([2, 3, 4]), 6), (array([3, 4, 5]), 7), (array([4, 5, 6]), 8), (array([5, 6, 7]), 9), (array([6, 7, 8]), 10), (array([7, 8, 9]), 11), (array([ 8,  9, 10]), 12)]
    """
    if stop is None:
        stop = np.inf
    if start is None:
        start = tail - 1

    idx_tail = np.arange(-tail + 1, 1)
    n = start
    if horizon is not None:
        stop -= horizon
        while n < stop:
            idx = idx_tail + n
            yield idx, idx[-1] + horizon
            n += step
    else:
        while n < stop:
            yield idx_tail + n
            n += step

def rollingtail(tail, *, stop, start=None, step=1, horizon=None):
    """ Indexes of a rolling tail

        Convenience function to allocate and organize outputs of
        :py:func:`.rollingtail_iter`.

        See documentation of :py:func:`.rollingtail_iter` for details.

        Examples
        --------
        >>> rollingtail(3, stop=10)
        array([[0, 1, 2],
               [1, 2, 3],
               [2, 3, 4],
               [3, 4, 5],
               [4, 5, 6],
               [5, 6, 7],
               [6, 7, 8],
               [7, 8, 9]])

        >>> rollingtail(3, stop=13, horizon=2)
        (array([[ 0,  1,  2],
               [ 1,  2,  3],
               [ 2,  3,  4],
               [ 3,  4,  5],
               [ 4,  5,  6],
               [ 5,  6,  7],
               [ 6,  7,  8],
               [ 7,  8,  9],
               [ 8,  9, 10]]), array([ 4,  5,  6,  7,  8,  9, 10, 11, 12]))
    """
    idx = list(rollingtail_iter(tail, horizon=horizon, start=start,
                    stop=stop, step=step))
    if horizon is not None:
        Xidx, yidx = tuple(zip(*idx))
        return np.array(Xidx), np.array(yidx)
    else:
        return np.array(idx)

def rolling_dfindex(df, window, **kwargs):
    """ Index range of rolling window

        Get the index range of a rolling window running on a
        :class:`pandas.DataFrame`.

        The range is obtanied by using the indexes min and max methods.

        Arguments
        ---------
        df: :class:`pandas.DataFrame`
            DataFrame on which the rolling window is applied
        window : int, or offset
            Size of the moving window. See :method:`pandas.DataFrame.rolling`
        kwargs: optional
            Arguments passed to :method:`pandas.DataFrame.rolling`

        Returns
        -------
        min, max: :class:`pandas.DataFrame`
            Minimum/Maximum of the index ranges in column "indexMin",
            with index given by the index of the input dataFrame.

        Notes
        -----
        This function can be useful to process many dataframes with the same index
        which cannot be concatenated due to memory constraints.
        Also it is useful to implement your own functions (e.g. not scalar) in
        a pandas rolling window.

        Examples
        --------
        >>> import pandas as pd
        >>> time = pd.to_timedelta([0,1,2,4,5,6,7,10,12,13], unit='m')
        >>> df = pd.DataFrame(list(range(10)), index=time, columns=['data'])
        >>> df
                  data
        00:00:00     0
        00:01:00     1
        00:02:00     2
        00:04:00     3
        00:05:00     4
        00:06:00     5
        00:07:00     6
        00:10:00     7
        00:12:00     8
        00:13:00     9

        >>> pd.concat(rolling_dfindex(df, '5min'), axis=1, sort=False)
                 indexMin indexMax
        00:00:00 00:00:00 00:00:00
        00:01:00 00:00:00 00:01:00
        00:02:00 00:00:00 00:02:00
        00:04:00 00:00:00 00:04:00
        00:05:00 00:01:00 00:05:00
        00:06:00 00:02:00 00:06:00
        00:07:00 00:04:00 00:07:00
        00:10:00 00:06:00 00:10:00
        00:12:00 00:10:00 00:12:00
        00:13:00 00:10:00 00:13:00

        Now you can compute any function on the rolling window on any dataframe
        with the same index

        >>> minmax = pd.concat(rolling_dfindex(df, '5min'), axis=1, sort=False)
        >>> pd.DataFrame([df[(df.index >= m) & (df.index <=M)].mean().values
        ...                                 for m,M in minmax.values],
        ...              columns=df.columns, index=df.index)
                  data
        00:00:00   0.0
        00:01:00   0.5
        00:02:00   1.0
        00:04:00   1.5
        00:05:00   2.5
        00:06:00   3.5
        00:07:00   4.5
        00:10:00   6.0
        00:12:00   7.5
        00:13:00   8.0

        >>> df.rolling('5min').mean()
                  data
        00:00:00   0.0
        00:01:00   0.5
        00:02:00   1.0
        00:04:00   1.5
        00:05:00   2.5
        00:06:00   3.5
        00:07:00   4.5
        00:10:00   6.0
        00:12:00   7.5
        00:13:00   8.0

        Another use case is model fitting (linear regression in
        this case)

        >>> import numpy as np
        >>> randn = np.random.randn
        >>> x = randn(10)
        >>> y = randn(10)
        >>> df = pd.DataFrame({'x':x, 'y':y, 'z':2 * x - y})
        >>> minmax = pd.concat(rolling_dfindex(df, 3), axis=1, sort=False)
        >>> coef = []
        >>> for m, M in minmax.values:
        ...     mask = (df.index >= m) & (df.index <=M)
        ...     coef.append(np.linalg.lstsq(df[['x','y']][mask], df['z'][mask],
        ...                                                      rcond=None)[0])
        >>> df['cx'] = [c[0] for c in coef]
        >>> df['cy'] = [c[1] for c in coef]
        >>> df                          #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
                  x         y         z   cx   cy
        0       ...       ...       ...  0.0  0.0
        1       ...       ...       ...  0.0  0.0
        2       ...       ...       ...  2.0 -1.0
        3       ...       ...       ...  2.0 -1.0
        4       ...       ...       ...  2.0 -1.0
        5       ...       ...       ...  2.0 -1.0
        6       ...       ...       ...  2.0 -1.0
        7       ...       ...       ...  2.0 -1.0
        8       ...       ...       ...  2.0 -1.0
        9       ...       ...       ...  2.0 -1.0

    """

    toidx = df.index.__class__

    rwin = df.rolling(window, **kwargs)
    # Use the first col
    c = df.columns[0]
    try:
        imin = rwin.apply(lambda x: x.index.values.min(), raw=False).apply(toidx)
        imax = rwin.apply(lambda x: x.index.values.max(), raw=False).apply(toidx)
    except TypeError:
        imin = rwin.apply(lambda x: x.index.values.min(), raw=False)
        imax = rwin.apply(lambda x: x.index.values.max(), raw=False)
    imin = imin[c]
    imin.name = 'indexMin'
    imax = imax[c]
    imax.name = 'indexMax'

    return imin, imax

if __name__ == '__main__':
    Nsamples = 100
    t = np.linspace(0, 1, Nsamples)
    X = np.sin(2 * np.pi * t)

    plt.ion()
    fig, ax = plt.subplots()
    ax.plot(X, '-k')
    xi, yi = 0, 0
    xh = ax.plot(xi, np.nan, 'om')
    yh = ax.plot(yi, np.nan, 'or')
    dt = t[1] - t[0]
    for xi, yi in rollingtail_iter(40, horizon=10, stop=None):
        if yi >= Nsamples:
            yi = yi % (Nsamples - 1)
            yh[0].set_ydata(np.sin(2 * np.pi * yi * dt))
        else:
            yh[0].set_ydata(X[yi])
        yh[0].set_xdata(yi)

        if xi[-1] >= Nsamples:
            xi = xi % (Nsamples - 1)
            xh[0].set_ydata(np.sin(2 * np.pi * xi * dt))
        else:
            xh[0].set_ydata(X[xi])
        xh[0].set_xdata(xi)

        fig.canvas.draw()
        print("xidx:{}\nyidx:{}\n".format(xi, yi))
        input("Press a key to show next slice")
