==============================
Python Package timeslice
==============================

Utilities to slice data sets along the samples axis (time series)

Installation
************

.. hint::
    To avoid collisions with your system's library versions,
    use a python virtual environment for installation.


::

    pip install "git+https://gitlab.com/kakila/timeslice.git"


Documentation
*************
The documentation is hosted at https://kakila.gitlab.io/timeslice

.. hint::
    To avoid collisions with your system's library versions,
    use a python virtual environment for installation.

To generate the documentation in a local copy of this repository,
do the following::

    pip install --upgrade -r requirements.txt

then::

    cd doc
    make html

and you will find the documentation in ``build/html/``
