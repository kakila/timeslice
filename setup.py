# vim: set tabstop=4
#!/usr/bin/env python3
""" setup script for timeslice module """

# Copyright (C) 2019 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

#' % Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#' % 08.07.2019

from os import path

from setuptools import (
    setup,
    find_packages
)

from timeslice import (
    __version__,
    __author__,
    __email__
)

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="timeslice",
    packages=find_packages(),
    version=__version__,
    install_requires=["matplotlib>=3.1.0"
                      "numpy>=1.16.4",
                      "scipy>=1.3.0",],
    author=__author__,
    author_email=__email__,
    url="https://gitlab.com/kakila/timeslice",
    description="Utilities to slice data sets along the samples axis (time series)",
    long_description=long_description,
    long_description_content_type='text/x-rst',
    classifiers=["Development Status :: 3 - Alpha",
                 "Environment :: Other Environment",
                 "Intended Audience :: Science/Research",
                 "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
                 "Operating System :: OS Independent",
                 "Programming Language :: Python :: 3",
                 "Topic :: Scientific/Engineering",
                 "Topic :: Scientific/Engineering :: Information Analysis",
                 ],
)
