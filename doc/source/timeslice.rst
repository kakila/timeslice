=================
API documentation
=================

Module: rolling
***************

.. automodule:: timeslice.rolling
   :members:
   :undoc-members:
   :show-inheritance:
