Welcome to Time slice's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   timeslice

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
